use std::{cmp::Ordering::Equal, collections::HashMap, error::Error, fs};

use comfy_table::{presets::NOTHING, ContentArrangement, Table};
use itertools::Itertools;
use serde_json::{Map, Value as JsonValue};

fn main() -> Result<(), Box<dyn Error>> {
    let f = fs::File::open("result.json")?;
    let root: JsonValue = serde_json::from_reader(f)?;
    let messages = root
        .as_object()
        .ok_or("Blah")?
        .get("messages")
        .ok_or("Blah")?
        .as_array()
        .ok_or("Blah")?;
    let mut results: HashMap<&str, UserStats> = HashMap::new();
    let mut usernames: HashMap<&str, &str> = HashMap::new();
    let mut total: usize = 0;
    for message in messages {
        total += 1;
        let Some(info) = get_msg_info(message) else {
            continue;
        };
        usernames.insert(info.user_id, info.username);
        let stats = results.entry(info.user_id).or_default();
        stats.total += 1;
        match info.msg_type {
            MsgType::Other => {}
            MsgType::Link => stats.link += 1,
            MsgType::Image => stats.image += 1,
            MsgType::Video => stats.video += 1,
            MsgType::Forward => stats.forward += 1,
        }
    }
    println!("Total messages: {total}");

    // Fix usernames
    usernames.insert("user2438877", "@bemyak");
    usernames.insert("user472080768", "@teleganapole");
    let results: HashMap<&str, UserStats> = results
        .into_iter()
        .map(|(k, v)| {
            let name = usernames.get(k).unwrap_or(&k);
            (*name, v)
        })
        .collect();

    let mut general = Table::new();
    general
        .load_preset(NOTHING)
        .set_content_arrangement(ContentArrangement::Dynamic)
        .set_header(vec![
            "Name", "Total", "Images", "Videos", "Links", "Forwards",
        ]);
    results
        .iter()
        .sorted_by(|a, b| b.1.total.cmp(&a.1.total))
        .for_each(|(k, v)| {
            general.add_row(vec![
                k.to_string(),
                v.total.to_string(),
                v.image.to_string(),
                v.video.to_string(),
                v.link.to_string(),
                v.forward.to_string(),
            ]);
        });
    println!("\n\nGeneral stats:\n{general}");

    let mut usefulness = Table::new();
    usefulness
        .load_preset(NOTHING)
        .set_content_arrangement(ContentArrangement::Dynamic)
        .set_header(vec!["Name", "% of media content"]);
    results
        .iter()
        .map(|(k, v)| {
            (
                k,
                (v.image + v.link + v.video + v.forward) as f64 / total as f64 * 100.,
            )
        })
        .sorted_by(|a, b| b.1.partial_cmp(&a.1).unwrap_or(Equal))
        .for_each(|(k, v)| {
            usefulness.add_row(vec![k.to_string(), format!("{v:.2}")]);
        });
    println!("\n\nUsefulness:\n{usefulness}");

    let mut reposters = Table::new();
    reposters
        .load_preset(NOTHING)
        .set_content_arrangement(ContentArrangement::Dynamic)
        .set_header(vec!["Name", "% reposts"]);
    results
        .iter()
        .map(|(k, v)| (k, v.forward as f64 / v.total as f64 * 100.))
        .sorted_by(|a, b| b.1.partial_cmp(&a.1).unwrap_or(Equal))
        .for_each(|(k, v)| {
            reposters.add_row(vec![k.to_string(), format!("{v:.2}")]);
        });
    println!("\n\nReposters:\n{reposters}");

    let mut ocs = Table::new();
    ocs.load_preset(NOTHING)
        .set_content_arrangement(ContentArrangement::Dynamic)
        .set_header(vec!["Name", "% original content"]);
    results
        .iter()
        .map(|(k, v)| (k, (v.image + v.video) as f64 / v.total as f64 * 100.))
        .sorted_by(|a, b| b.1.partial_cmp(&a.1).unwrap_or(Equal))
        .for_each(|(k, v)| {
            ocs.add_row(vec![k.to_string(), format!("{v:.2}")]);
        });
    println!("\n\nOriginal Content:\n{ocs}");

    Ok(())
}

fn get_msg_info(m: &JsonValue) -> Option<MsgGist> {
    let Some(m) = m.as_object() else {
        return None;
    };
    let user_id = m.get("from_id")?.as_str()?;
    let username = m.get("from")?.as_str()?;
    let msg_type: MsgType = m.into();
    Some(MsgGist {
        user_id,
        username,
        msg_type,
    })
}

struct MsgGist<'a> {
    user_id: &'a str,
    username: &'a str,
    msg_type: MsgType,
}

#[derive(Default)]
struct UserStats {
    total: usize,
    image: usize,
    video: usize,
    link: usize,
    forward: usize,
}

enum MsgType {
    Other,
    Link,
    Image,
    Video,
    Forward,
}

impl From<&Map<String, JsonValue>> for MsgType {
    fn from(value: &Map<String, JsonValue>) -> Self {
        if value.get("forwarded_from").is_some() {
            return MsgType::Forward;
        }
        if value.get("photo").is_some() {
            return MsgType::Image;
        }
        if let Some((t, _)) = value
            .get("mime_type")
            .and_then(JsonValue::as_str)
            .and_then(|mime_type| mime_type.split_once('/'))
        {
            match t {
                "video" => return MsgType::Video,
                "image" => return MsgType::Image,
                _ => {}
            }
        }

        if Some(true) == value
            .get("text_entities")
            .and_then(JsonValue::as_array)
            .map(|arr| {
                arr.iter()
                    .filter_map(JsonValue::as_object)
                    .any(|v| v.get("type").and_then(|v| v.as_str()) == Some("link"))
            })
        {
            return MsgType::Link;
        }

        MsgType::Other
    }
}
